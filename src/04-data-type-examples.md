## More Haskell Syntax { data-transition="fade none" }

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Person = Person Int String
</code></pre>

* <code>Person</code> is a data type with 1 constructor
* That constructor has two fields

<pre><code class="language-haskell hljs" data-trim data-noescape>
> :type Person
Person :: Int -> String -> Person
</code></pre>

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
</code></pre>

* <code>Optional</code> is a data type with 2 constructors
* The data type has 1 polymorphic _type parameter_

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
</code></pre>

* Let's write a function on the <code>Optional</code> data type
* <code>mapOptional :: (a -> b) -> Optional a -> Optional b</code>

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
</code></pre>

* All <code>Optional</code> values have been constructed with either <code>Empty</code> or <code>Full</code>
* We want to return a value that depends on which constructor was used
* We want to _pattern-match_

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a

mapOptional :: (a -> b) -> Optional a -> Optional b
mapOptional func optl =
  case optl of
    Empty -> _
    Full x -> _
</code></pre>

* Note that we have named any fields (or arguments) of the constructors
* <code>Full</code> has one argument, so we named it <code>x</code>

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
  deriving (Eq, Show)
</code></pre>

* Add <code>deriving</code> so that you can experiment with values at GHCi
* Practice by writing this example function:
* <code>bindOptional :: (a -> Optional b) -> Optional a -> Optional b</code>

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

* <code>List</code> is a _recursive_ data type
* Specifically its definition is given in terms of itself

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

* <code>List</code> has two constructors (<code>Nil</code> and </code>Cons</code>)
* Any list can be defined using these two constructors

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

* For example, the list <code>[1, 2, 3]</code>
* <code>Cons 1 (Cons 2 (Cons 3 Nil))</code>

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

* Practice with GHCi
* Define the <code>List</code> data type
* Add <code>deriving</code> for <code>Eq</code> and <code>Show</code>
* What is the type of <code>Nil</code> and <code>Cons</code>?
* Define some <code>List</code> values by calling the constructor(s)
* Write these functions
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  -- adds up the numbers in a list
  sum :: List Integer -> Integer
  -- appends two lists
  (++) :: List x -> List x -> List x
  -- flattens a list of lists
  flatten :: List (List x) -> List x
  -- reverses a list
  reverse :: List x -> List x
  </code></pre>
  
---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Or a b = IsA a | IsB b
</code></pre>

* <code>Or</code> is a data type with two constructors
* Defined over two polymorphic type variables

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Or a b = IsA a | IsB b
</code></pre>

* Practice with GHCi
* Define the <code>Or</code> data type
* Add <code>deriving</code> for <code>Eq</code> and <code>Show</code>
* What is the type of <code>IsA</code> and <code>IsB</code>?
* Define some <code>Or</code> values by calling the constructor(s)
* Write these functions
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  modifyA :: (a -> a) -> Or a b -> Or a b
  getB :: Or a b -> Optional b
  </code></pre>
  
---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Tree a = Tree a (List (Tree a))
</code></pre>

* <code>Tree</code> is a recursive data type with one constructor
* The constructor has two values:
  * The root tree node
  * The children 
* This data type denotes a _rose tree_ aka _multi-way tree_

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Tree a = Tree a (List (Tree a))
</code></pre>
* Practice with GHCi
* Define the <code>Tree</code> data type
* Add <code>deriving</code> for <code>Eq</code> and <code>Show</code>
* What is the type of <code>Tree</code>?
* Define some <code>Tree</code> values by calling the constructor
* Write these functions
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  mapTree :: (a -> b) -> Tree a -> Tree b
  traverseTreeOptional ::
    (a -> Optional b) -> Tree a -> Optional (Tree b)
  </code></pre>
  
---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)
</code></pre>

* <code>ParseResult</code> is a data type with two constructors
* <code>Parser</code> is a data type with one constructor
* The <code>Parser</code> accepts one argument, which is a function

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)
</code></pre>

* It may be said that a parser for <code>x</code> is a function from the string input to either:
  * an error message
  * a value of type <code>x</code> and a string of the remaining input

---

###### Examples of Data Types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ParseResult x =
  ParseError String | ParseSuccess x String
data Parser x = Parser (String -> ParseResult x)
</code></pre>

* Practice with GHCi
* Define the <code>ParseResult</code> data type and <code>Parser</code> data type
* Add <code>deriving</code> for <code>Eq</code> and <code>Show</code>
* Define some <code>ParseResult</code> and <code>Parser</code> values by calling the constructors
* Write these functions
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  -- the error message
  parseError :: ParseResult x -> Optional String
  -- return a parser that succeeds, puts the input to the output
  neutralParser :: x -> Parser x
  -- a parser that consumes one character of input (if it can)
  character :: Parser Char
  </code></pre>

