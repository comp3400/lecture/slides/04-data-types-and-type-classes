## More Haskell Syntax { data-transition="fade none" }

###### Pattern matching

* Working with data constructors requires _pattern matching_
* Separate line of code for each constructor
* Pattern matching starts with `case` keyword
* Next is a value you want to match on, followed by the `of` keyword
* Then comes 1 or more cases with an `->` arrow, followed by the return value for that pattern

<pre><code class="language-haskell hljs" data-trim data-noescape>
showBool :: Bool -> String
showBool b =
  case b of
    True ->
      "True"
    False ->
      "False"
</code></pre>

---

###### Pattern matching

* Patterns can give names to the constructor's fields

<pre><code class="language-haskell hljs" data-trim data-noescape>
sumTree :: Tree Int -> Int
sumTree t =
  case t of
    Leaf ->
      0
    Branch left a right ->
      sumTree left + a + sumTree right
</code></pre>

---

###### Pattern matching

* Patterns can include `_` underscores for fields where the value is not required for the return value

<pre><code class="language-haskell hljs" data-trim data-noescape>
personName :: Person -> String
personName p =
  case p of
    Person name _ ->
      name
</code></pre>

---

###### Pattern matching

* Functions that pattern match on arguments are very common
* So Haskell provides _syntactic sugar_
* Each pattern-match in a function definition, for each constructor, appears on a new line

<pre><code class="language-haskell hljs" data-trim data-noescape>
sumTree :: Tree Int -> Int
sumTree Leaf =
  0
sumTree (Branch left a right) =
  sumTree left + a + sumTree right
</code></pre>

