## More Haskell Syntax { data-transition="fade none" }

###### Type classes

* Fairly unique feature to Haskell
* Allows extending functions
* A _class_ specifies _member_ functions
* _Instances_ specify implementations for a particular data type

---

###### Type classes

* Following the <code>class</code> keyword, is the _name_ of the type class
* Next is the name of a type variable, then the <code>where</code> keyword
* Next are the type-signatures _(indented)_ for the 1 or more _methods_ of the type class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Show a where
  show :: a -> String

class Eq a where
  (==) :: a -> a -> Bool
</code></pre>

---

###### Type classes

* Following the <code>instance</code> keyword, is the _name_ of the type class
* Next is the name of a data type, then the <code>where</code> keyword
* Next are the implementations for the 1 or more _methods_ of the type class

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bit
  = Zero
  | One

instance Show Bit where
  show Zero = "Zero"
  show One  = "One"

instance Eq Bit where
  (==) Zero Zero = True
  (==) One  One  = True
  (==) _    _    = False
</code></pre>

---

###### Type classes

* Methods can be used with any data type with an instance

<pre><code class="language-plain hljs" data-trim data-noescape>
Prelude> Zero == One
False
Prelude> 'a' == 'a'
True
Prelude> show Zero
"Zero"
Prelude> show True
"True"
</code></pre>

---

###### Type classes

* Methods can not be used with a data type without an instance

<pre><code class="language-plain hljs" data-trim data-noescape>
Prelude> data X = X
Prelude> show X

&lt;interactive&gt;:2:1: error:
    • No instance for (Show X) arising from a use of ‘show’
    • In the expression: show X
      In an equation for ‘it’: it = show X
Prelude> X == X

&lt;interactive&gt;:3:1: error:
    • No instance for (Eq X) arising from a use of ‘==’
    • In the expression: X == X
      In an equation for ‘it’: it = X == X
</code></pre>

---

###### Type classes

* There are some rules for type classes
* There can only be at most 1 instance of each type class for a data type
* An instance must be in one of two modules:
  1. the same module in which the data type is defined
  2. the same module in which the type class is defined

---

###### Type classes

* You can ask ghci "which data types have an instance for this type class?"

<pre><code class="language-plain hljs" data-trim data-noescape>
Prelude> :info Show
class Show a where
  show :: a -> String
instance (Show a, Show b) => Show (Either a b)
  -- Defined in ‘Data.Either’
instance Show a => Show [a] -- Defined in ‘GHC.Show’
instance Show Word -- Defined in ‘GHC.Show’
instance Show GHC.Types.RuntimeRep -- Defined in ‘GHC.Show’
instance Show Ordering -- Defined in ‘GHC.Show’
instance Show a => Show (Maybe a) -- Defined in ‘GHC.Show’
instance Show Integer -- Defined in ‘GHC.Show’
instance Show Int -- Defined in ‘GHC.Show’
instance Show Char -- Defined in ‘GHC.Show’
instance Show Bool -- Defined in ‘GHC.Show’
</code></pre>

---

###### Type classes

* Some base Haskell classes allow _deriving_ an instance
* Haskell implements these instances using simple rules

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bit
  = Zero
  | One
  deriving (Eq, Show)
</code></pre>

---

###### Type classes

* What is the type of `show` or `(==)`?

<pre><code class="language-plain hljs" data-trim data-noescape>
Prelude> :type show
show :: Show a => a -> String
Prelude> :type (==)
(==) :: Eq a => a -> a -> Bool
</code></pre>

---

###### Type classes

* A function type-signature can have _constraints_
* A constraint:
  * appears left-most in the type signature
  * is notated using a *fat arrow* `=>`
  * has one or more type class names, followed by a type-variable
  * each type class constraint is separated by a comma and surrounded in parentheses

<pre><code class="language-haskell hljs" data-trim data-noescape>
showEqual :: (Show a, Eq a) => a -> a -> String
showEqual a b =
  if a == b
  then
    show a ++ " is equal to " ++ show b
  else
    "different"
</code></pre>

---

###### Type classes

* A type class can have constraints
* These are called _superclass constraints_
* To write an instance, you first must have instances for the superclasses

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Eq a => Ord a where
  (&lt;=) :: a -> a -> Bool
</code></pre>

---

###### Type classes

* Superclasses are automatically expanded in constraints
* It's legal Haskell to be redundant but discouraged

<pre><code class="language-haskell hljs" data-trim data-noescape>
ordCanEq :: Ord a => a -> a -> Bool
ordCanEq a b = a == b
</code></pre>

---

###### Type classes

* Haskell uses the `class` keyword but it's very different to other languages
* More similar to an _interface_ but not the same
* Be open to differences!

