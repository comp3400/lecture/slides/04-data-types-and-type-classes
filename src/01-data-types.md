## More Haskell Syntax { data-transition="fade none" }

###### Data types

* We have seen built-in data types, such as <code>Char</code>
* Sometimes we wish to create our own data type
* Data types are declared using the <code>data</code> keyword

---

###### Data types

* Following the <code>data</code> keyword, is the <em>name</em> of the data type
* Next are 0 or more type variables, then an equals (<code>=</code>) sign
* For example
  * <code class="language-haskell hljs">data Person =</code>
  * <code class="language-haskell hljs">data Tree a =</code>
  * <code class="language-haskell hljs">data Pair a b =</code>
  * <code class="language-haskell hljs">data Error =</code>

---

###### Data types

* Following the equals (<code>=</code>) sign, are 0 or more <strong>data constructors</strong>
* These data constructors are separated by a pipe (<code>|</code>) symbol
* Following each data constructor are 0 or more <strong>constructor arguments</strong>
* For example
  * <code class="language-haskell hljs">data Person = Person String Int</code>
  * <code class="language-haskell hljs">data Tree a = Leaf | Node (Tree a) a (Tree a)</code>
  * <code class="language-haskell hljs">data Pair a b = Pair a b</code>
  * <code class="language-haskell hljs">data DivideByZeroError = Division Float | ...</code>

---

###### Data types

* It might be said that the number of data constructors for a data type is one plus the number of pipe symbols

---

###### Data types

* Constructors can be used as functions
* The result is a value of the data type

<pre><code class="language-plain hljs" data-trim data-noescape>
Prelude> :type Person
Person :: String -> Int -> Person
Prelude> :type Leaf
Leaf :: Tree a
Prelude> :type Node
Node :: Tree a -> a -> Tree a -> Tree a
</code></pre>

