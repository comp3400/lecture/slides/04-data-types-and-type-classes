## More Haskell Syntax { data-transition="fade none" }

###### Code Repetition and Type Classes

* Suppose these functions already exist and have been written
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  mapList ::     (a -> b) -> List     a -> List     b
  mapOptional :: (a -> b) -> Optional a -> Optional b
  mapParser ::   (a -> b) -> Parser   a -> Parser   b
  mapVector6 ::  (a -> b) -> Vector6  a -> Vector6  b
  </code></pre>

---

###### Code Repetition and Type Classes

* We then write this function for <code>List</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipList :: List (a -> b) -> a -> List b
flipList list a =
  mapList (\func -> func a) list
</code></pre>

---

###### Code Repetition and Type Classes

* and then for <code>Optional</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipOptional :: Optional (a -> b) -> a -> Optional b
flipOptional opt a =
  mapOptional (\func -> func a) opt
</code></pre>

---

###### Code Repetition and Type Classes

* and then for <code>Parser</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipParser :: Parser (a -> b) -> a -> Parser b
flipParser prsr a =
  mapParser (\func -> func a) prsr
</code></pre>

---

###### Code Repetition and Type Classes

* and then for <code>Vector6</code>

<pre><code class="language-haskell hljs" data-trim data-noescape>
flipVector6 :: Vector6 (a -> b) -> a -> Vector6 b
flipVector6 v6 a =
  mapVector6 (\func -> func a) v6
</code></pre>

---

###### Code Repetition and Type Classes

* What might we consider doing? What would that code look like?
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  flipList ::
    List (a -> b) -> a -> List b
  flipList list a =
    mapList (\func -> func a) list

  flipOptional ::
    Optional (a -> b) -> a -> Optional b
  flipOptional opt a =
    mapOptional (\func -> func a) opt

  flipParser ::
    Parser (a -> b) -> a -> Parser b
  flipParser prsr a =
    mapParser (\func -> func a) prsr

  flipVector6 ::
    Vector6 (a -> b) -> a -> Vector6 b
  flipVector6 v6 a =
    mapVector6 (\func -> func a) v6
  </code></pre>

