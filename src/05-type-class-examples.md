## More Haskell Syntax { data-transition="fade none" }

###### Examples of Type Classes

* Suppose we wanted to define a function to check if it contains an element
* <code>x -> List x -> Bool</code>

---

###### Examples of Type Classes

* <code>x -> List x -> Bool</code>
* However, to achieve this, we need to test the elements for _equality_
* This type signature alone is not enough
* We need to _constrain_ the element type with a type class

---

###### Examples of Type Classes

* Although there already exists an <code>Eq</code> type class in base
* We will write our own to demonstrate

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Equal a where
  isEqual :: a -> a -> Bool
</code></pre>

* A type class called <code>Equal</code> with a single abstract method called <code>isEqual</code>
* The <code>isEqual</code> method determines if two values are equal

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
instance Equal Char where
  isEqual 'a' 'a' = True
  isEqual 'A' 'A' = True
  isEqual 'x' _ = True
  isEqual _ _ = False
</code></pre>

* A silly instance for <code>Char</code> that returns <code>True</code> under some conditions
* There can only be at most **one** instance of <code>Equal</code> for <code>Char</code>
* Therefore, this is that instance!

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
contains :: Equal x => x -> List x -> Bool
contains elem Nil = False
contains elem (Cons hd tl) =
  isEqual elem hd then ||
  contains elem tl
</code></pre>

* We have constrained our elements with the type class <code>Equal</code>
* Now we can use the <code>isEqual</code> method on element types

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
> contains 'a' Nil
False

> contains 99 Nil -- error, no instance of Equal for Integer
</code></pre>

* We can use <code>contains</code> with element types that have an <code>instance</code>
* If there is no instance for that type, we'd see a _type error_

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Ordering = LessThan | EqualTo | GreaterThan
class Eq x => Order x where
  compare :: x -> x -> Ordering
</code></pre>

* We could define another type class <code>Order</code> that compares two values for ordering
* We have _constrained_ the <code>Order</code> type class with the <code>Equal</code> type class
* We could only write an <code>instance</code> of a data type for <code>Order</code> if <code>instance Equal</code> has also been defined

---

###### Examples of Type Classes

* These two type classes already exist in the base library
* Called <code>Eq</code> and <code>Ord</code>
* Have a look at their definitions (<code>:info</code>)

---

###### Examples of Type Classes

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vector6 a = Vector6 a a a a a a
</code></pre>

* Practice with some examples
* Write these functions
  <pre><code class="language-haskell hljs" data-trim data-noescape>
  maximumV6 :: Order a => Vector6 a -> a
  reverseV6 :: Order a => Vector6 a -> Vector6 a
  </code></pre>
* Do both these functions require the type class constraint? Why or why not?

